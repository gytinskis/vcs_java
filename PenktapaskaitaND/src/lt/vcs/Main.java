/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedMap;

/**
 *
 * @author vartotojas
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        out("iveskite norima teksta");
        letterCounter();
    }
    
    public static void letterCounter(){
        String word = inWords().toUpperCase(); // ivedamas tekstas ir paverciamas i didzeses raides
        word= word.replace(" ", "");// panaikina visus tarpus is teksto
        word = word.replace(",","");
        char[] letter = word.toCharArray(); // pavercia visa teksta i char masyva
        Map<Character,Integer> mapas = new HashMap(); //  sukuriamas zodynas(map)  pavadinimu mapas
        
        for( int i=0;i<word.length();i++){
            if(!mapas.containsKey(letter[i])){ // tikrinama ar mapas zodyne yra raktas (raide)
                mapas.put(letter[i],1); //  jei nera tada pridedama naujas mapas raktas
            }
            else{
                mapas.put(letter[i],mapas.get(letter[i])+1); // jei jau yra toks raktas tada jo reiksmei pridedamas 1
            }
             
        }
        
        out("" + mapas); // isvedamas atsakymas i console
    }
    public static void out(String txt){
        System.out.println(txt);
    }
    public static Scanner scan(){
        return new Scanner(System.in);
    }
    public static String inWords(){
        return scan().nextLine();
    }
    public static int inInt(){
        return scan().nextInt();
    }
}
