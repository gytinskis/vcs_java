/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static lt.vcs.VcsUtils.*;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String line = inLine("iveskite zodziu atskirtus kableliu");
        line = line.replace(" ", "");
        String[] lineMas = line.split(",");
        List<String> lineList = createList(lineMas);
        Set<String> lineSet = createSet(lineMas);

        out("-------cia bus linelist--------");
        outCollection(lineList);
        out("-------cia bus lineSet--------");
        outCollection(lineSet);
        out("------lineList SORTED--------");
        Collections.sort(lineList);
        outCollection(lineList);
        out("------lineList SORTED reverse--------");
        Collections.reverse(lineList);
        outCollection(lineList);
        Map<String, Integer> mapas = new HashMap(); // maps yra kaip C# dictionary turi rakta ir  jam priskirta reiksme
        mapas.put("obuolys", 5); // nustatotmas obuolys yra raktas o  5 reiksme 
        Integer val = mapas.get("obuolys"); // val  tampa raktas obuolys
        out(val);
        Set<String> raktai = mapas.keySet(); // parodo visus raktus
        out(mapas.keySet());
        Collection<Integer> reiksmes = mapas.values();
        for (Integer reiksme : reiksmes) {

        }
        Player p1 = null;
        while (p1 == null) {
            try {
                p1 = new Player(inWord("ivesk varda"), inWord("iStrivesk emaila"));
            } catch (Exception e) {
                out(e.getMessage());
            }
        }
    }
    
    private static Set<String> createSet(String... string){
        Set<String>result = new HashSet();
        if(string!= null){
            result.addAll(Arrays.asList(string));
        }
        return result;
    }
    private static List<String> createList(String...string){
        List<String> result = new ArrayList();
        if(string != null){
            result.addAll(Arrays.asList(string));
        }
        return result;
    }
    
    private static void outCollection(Collection col){
        if(col!=null){
        for(Object item :col){
            out(item);
        }
        }
    }
}
