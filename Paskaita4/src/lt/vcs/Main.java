/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author vartotojas
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Player p1= new Player(inWord("iveskite varda"),Gender.getById(inInt("iveskite lyti")));
        Person per1 = new Person("zmogenas");
        Object obj= p1;
        per1=p1;
        
        out(p1);
        out(per1);
        out(obj);
        out(new Object());
        
        
        User<Person> u1= new User(p1);
        User<Player> u2= new User(p1);
        out(u1.getPerson());
        out(u2.getPerson());
        
        

        
        List<String> strList = new ArrayList();
        strList.add("bla");
        strList.add("bla");
        for( String bla: strList){
            out(bla);
        }
        out("-------------------set metodas");
        Set<String> strSet = new HashSet();
        strSet.add("bla");
        strSet.add("bla");
        for( String bla: strSet){
        out(bla);
        }
    }
    
}
